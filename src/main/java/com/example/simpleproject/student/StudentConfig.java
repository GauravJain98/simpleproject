package com.example.simpleproject.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student gaurav = new Student("Gaurav Jain", "gauravcdj.98@gmail.com", LocalDate.of(1998, Month.SEPTEMBER, 30));
            Student sam = new Student("Sam", "sam@gmail.com", LocalDate.of(1998, Month.SEPTEMBER, 30));
            repository.saveAll(List.of(gaurav, sam));
        };
    }

}
